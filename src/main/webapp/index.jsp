<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>HelloWorld</title>
  <style>
    h2{
      position:absolute;
      width:100%;
      top:50%;
      text-align:center;
      font-style: italic;
    }
  </style>
</head>
<body>
<div>
  <div>
    <form action="/hellogreetgo/controller" method="post">
      <div>
        <input type="text" name="text">
      </div>
      <div>
        <input type="submit" value="Change Text">
      </div>
    </form>
  </div>
  <div>
    <h2 id="text"></h2>
  </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $.ajax({
        type: 'GET',
        url: '/hellogreetgo/controller',
        dataType: 'json',
        success: function (data) {
            $("#text").append(data.text);
        }
    });
</script>
</html>
