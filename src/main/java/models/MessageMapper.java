package models;

import org.apache.ibatis.annotations.Param;

public interface MessageMapper {
    Message getMessageById(int index);
    void updateMessageById(@Param("id") int id, @Param("text") String text);
}
