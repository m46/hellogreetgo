package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Message;
import services.MessageService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller")
public class MessageController extends HttpServlet {
    MessageService messageService = new MessageService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Message message = messageService.getMessageById(1);
        resp.setContentType("application/json");
        Gson gson = new GsonBuilder().create();
        try {
            resp.getWriter().write(gson.toJson(message) + "\n");
            resp.getWriter().flush();
            resp.getWriter().close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String newText = req.getParameter("text");
        messageService.updateMessageById(1,newText);
        try {
            resp.sendRedirect("/hellogreetgo");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
