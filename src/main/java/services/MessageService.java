package services;

import models.Message;
import models.MessageMapper;
import org.apache.ibatis.session.SqlSession;
import util.MyBatisSqlSessionFactory;

public class MessageService {
    public Message getMessageById(int index){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try {
            MessageMapper mapper = sqlSession.getMapper(MessageMapper.class);
            return mapper.getMessageById(index);
        }finally {
            sqlSession.close();
        }
    }

    public void updateMessageById(int id, String newText){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try {
            MessageMapper mapper = sqlSession.getMapper(MessageMapper.class);
            mapper.updateMessageById(id, newText);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }
}
